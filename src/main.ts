import stream from 'stream';
import express from 'express';
import {RequestHandler, Request} from 'express';
import bodyParser from 'body-parser';
import request from 'superagent';
import os from 'os';


type RewritePattern = [RegExp | string, string];
interface IProxyDef {
    frontend: {
        port: number;
    };
    backend: {
        protocol: 'http' | 'https';
        hostname: string;
        port: number;
    };
}


const FRONTEND_HOSTNAME = os.hostname();
const PROXIES: IProxyDef[] = [
    {
        frontend: {
            port: 3000,
        },
        backend: {
            protocol: 'http',
            hostname: 'docker-machine.local',
            port: 8000,
        },
    },
    {
        frontend: {
            port: 3001,
        },
        backend: {
            protocol: 'http',
            hostname: 'docker-machine.local',
            port: 8080,
        },
    },
];
const REWRITE_CONTENT_TYPES = [
    'text/html',
];
const HEADER_BLACKLIST = [
    'content-encoding',
    'transfer-encoding',
    'connection',
    'content-length',
];



class RewriteContentStream extends stream.Transform {
    disable_rewrite: boolean | undefined;
    _patterns: RewritePattern[] | undefined;
    _charset: string | undefined;
    _buffer: Buffer | undefined;

    private buildRewritePatterns () {
        const rewritePatterns: RewritePattern[] = [];
        // Full URL
        for (const proxyDef of PROXIES) {
            rewritePatterns.push([
                `${proxyDef.backend.protocol}://${proxyDef.backend.hostname}:${proxyDef.backend.port}`,
                `http://${FRONTEND_HOSTNAME}:${proxyDef.frontend.port}`,
            ]);
        }
        // Missing proto
        for (const proxyDef of PROXIES) {
            rewritePatterns.push([
                `${proxyDef.backend.hostname}:${proxyDef.backend.port}`,
                `${FRONTEND_HOSTNAME}:${proxyDef.frontend.port}`,
            ]);
        }
        // Just hostname
        for (const proxyDef of PROXIES) {
            rewritePatterns.push([
                `${proxyDef.backend.hostname}`,
                `${FRONTEND_HOSTNAME}`,
            ]);
        }
        return rewritePatterns;
    }

    private getRewritePatterns () {
        if (!this._patterns) {
            this._patterns = this.buildRewritePatterns();
        }
        return this._patterns;
    }

    private rewriteChunk (chunk: string) {
        const rewritten = this.getRewritePatterns()
            .reduce((memo, [pattern, replacement]) => {
                return memo.replace(pattern, replacement);
            }, chunk);
        return rewritten;
    }

    private rewriteBuffer (chunk: Buffer) {
        if (!this._charset) {
            return chunk;
        }
        const inString = chunk.toString(this._charset);
        const outString = this.rewriteChunk(inString);
        return Buffer.from(outString);
    }

    setCharset (charset: string) {
        this._charset = charset
            ? charset.toLowerCase().replace(/[^a-z0-9]/g, '')
            : undefined;
    }

    _transform (chunk: Buffer, _: string, cb: stream.TransformCallback) {
        if (this._buffer) {
            this._buffer = Buffer.concat([this._buffer, chunk]);
        } else {
            this._buffer = chunk;
        }
        cb();
    };

    _flush (cb: stream.TransformCallback) {
        if (this.disable_rewrite) {
            this.push(this._buffer);
        } else {
            this.push( this._buffer ? this.rewriteBuffer(this._buffer) : undefined );
        }
        cb();
    }
}


function buildBackendRequestHeaders(proxyDef: IProxyDef, request: Request) {
    const reqHeaders: Request['headers'] = {
        ...request.headers,
        host: `${proxyDef.backend.hostname}:${proxyDef.backend.port}`,
    };
    for (const badHeader of HEADER_BLACKLIST) {
        delete reqHeaders[badHeader];
    }
    return reqHeaders;
}


function cleanResponseHeaders(response: request.Response) {
    const respHeaders = {
        ...response.header,
    };
    for (const badHeader of HEADER_BLACKLIST) {
        delete respHeaders[badHeader];
    }
    return respHeaders;
}


function buildProxyHandler(proxyDef: IProxyDef) {
    const proxyRequest: RequestHandler = async function(feReq, feResp) {
        const reqPath = feReq.params['0'];
        const beURL = `${proxyDef.backend.protocol}://${proxyDef.backend.hostname}:${proxyDef.backend.port}${reqPath}`;
        const beMethod = feReq.method;
        const beHeaders = buildBackendRequestHeaders(proxyDef, feReq);
        const beBody = feReq.body;

        console.log(`${beMethod} ${beURL} [Starting]`);
        const pendingRequest = request(beMethod, beURL)
                .set(beHeaders)
                .query(feReq.query)
                .send(beBody);

        // Create a content rewriting stream
        const rewriteContent = new RewriteContentStream();

        // Set response headers
        pendingRequest.on('response', (beResp: request.Response) => {
            console.log(`${beMethod} ${beURL} [Received reply with status ${beResp.status}]`);
            rewriteContent.setCharset(beResp.charset);  // Set the charset so that the transform stream can rewrite content

            const contentType = ((beResp.header['content-type'] || '') as string)
                .toLowerCase()
                .split(';')
                .shift();
            rewriteContent.disable_rewrite = contentType
                ? (REWRITE_CONTENT_TYPES.indexOf(contentType) === -1)
                : true;

            feResp.status(beResp.status);
            feResp.set(cleanResponseHeaders(beResp))
        });

        // Set response end handler
        pendingRequest.on('end', () => {
            console.log(`${beMethod} ${beURL} [Finished]`);
        });

        // Pipe backend response content to the frontend response, using the content write stream middleware
        rewriteContent.pipe(feResp);
        pendingRequest.pipe(rewriteContent);
    };
    return proxyRequest;
}


function startProxy(proxyDef: IProxyDef) {
    const handleProxyRequest = buildProxyHandler(proxyDef);
    const app = express();
    app.use(bodyParser.urlencoded({ extended: false }))
    app.use(bodyParser.json())
    app.use(bodyParser.text())
    app.all(/^(\/.*)/, handleProxyRequest);
    app.listen(proxyDef.frontend.port, () => {
        console.log(`Listening on http://${FRONTEND_HOSTNAME}:${proxyDef.frontend.port}/ with backend ${proxyDef.backend.protocol}://${proxyDef.backend.hostname}:${proxyDef.backend.port}/`);
    });
    return app;
}


function main() {
    for (const proxyDef of PROXIES) {
        startProxy(proxyDef);
    }
}


main();
