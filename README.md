# local-dev-proxy

## What?

This is a simple HTTP proxy built to make local development servers (like docker containers running in a virtual machine with a host-only network) accessible to mobile devices on the same local-area-network. This makes it much easier to develop features locally, while still testing on a real mobile device.

## How?

Made sure you have [Git](https://git-scm.com/), [Node.js](https://nodejs.org/en/), and [Yarn](https://yarnpkg.com/lang/en/) installed. On OS X, you can do this using [Homebrew](https://brew.sh/).

```bash
$ brew install git node yarn
```

Clone this repository.

```bash
$ git clone git@gitlab.com:thelabnyc/local-dev-proxy.git
$ cd local-dev-proxy
```

Install the dependencies using Yarn.

```bash
$ yarn
```

Run the proxy. The first time you do this, your Firewall will most likely ask whether or not to allow incoming connections to the process. Make sure you allow this.

```bash
$ yarn run start
Listening on http://mycomputer.local:3000/ with backend http://docker-machine.local:8000/
Listening on http://mycomputer.local:3001/ with backend http://docker-machine.local:8080/
```

You may now access your local development project on a mobile device (on the same local network) by accessing `http://mycomputer.local:3000/` (replace mycomputer.local with your computer's hostname) in it's web browser.

#### DNS Hostname Issues

In some cases DNS resolution to your computer's hostname doesn't work. For example, when you try and load http://mycomputer.local:3000/ either on your mobile deivce or directly on your computer, you may receive an error like, "*mycomputer.local's server IP address could not be found.*" When this is the case, you'll need to make the proxy use your local IP address directly. First, find your computer's local IP address.

```bash
$ ifconfig | grep 'inet ' | grep -v 'inet 127.'
	inet 10.0.1.143 netmask 0xffff0000 broadcast 10.0.255.255
	inet 10.0.1.159 netmask 0xffff0000 broadcast 10.0.255.255
```

If that command outputs multiple IP addresses (like above), start with the first IP address. Next, open `src/main.ts` and find the line `const FRONTEND_HOSTNAME = os.hostname();`. Replace the call to `os.hostname()` with your IP address as a string. For example:

```ts
const FRONTEND_HOSTNAME = '10.0.1.143';
```

Save and close the file, then restart the proxy. This time, it should say that it's listening on the IP address you entered.

```bash
$ yarn run start
Listening on http://10.0.1.143:3000/ with backend http://docker-machine.local:8000/
Listening on http://10.0.1.143:3001/ with backend http://docker-machine.local:8080/
```

Finally, you can attempt to load your build using your IP address (like http://10.0.1.143:3000/).

## Details

By default the proxy forwards two ports:

- Host port `3000` proxies traffic to `http://docker-machine.local:8000/`
- Host port `3001` proxies traffic to `http://docker-machine.local:8080/`

This is what we normally use for local development at thelabnyc. You can change the proxy backend settings or add more proxied backends by editing `PROXIES` constant in `src/main.ts`. It's default value is:

```typescript
const PROXIES: IProxyDef[] = [
    {
        frontend: {
            port: 3000,
        },
        backend: {
            protocol: 'http',
            hostname: 'docker-machine.local',
            port: 8000,
        },
    },
    {
        frontend: {
            port: 3001,
        },
        backend: {
            protocol: 'http',
            hostname: 'docker-machine.local',
            port: 8080,
        },
    },
];
```

You can also control which content-types are rewritten and which headers to strip from requests and responses using the contants `REWRITE_CONTENT_TYPES` and `HEADER_BLACKLIST`. By default the look like this:

```typescript
const REWRITE_CONTENT_TYPES = [
    'text/html',
];
const HEADER_BLACKLIST = [
    'content-encoding',
    'transfer-encoding',
    'connection',
    'content-length',
];
```

Content rewriting processes the response body after receiving it from the backend, but before passing it onto the client. It looks for any links that reference the backend URL or hostname directly (like `http://docker-machine.local:8000/foo/`), and rewrites them then the proxy's hostname (like `http://mycomputer.local:3000/foo/`). This ensures that even absolute links to images and other assets continue to function through the proxy.
